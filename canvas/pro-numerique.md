# Canvas
Lean canvas Villages en Commun pour pro du numérique

# Problem
1. Pas de base arrière
2. Manque de sens dans le travail
3. Déterritorialisation de l'activité professionnelle

# Customer-segment
1. Développeur itinérant

# Unique-Value-Proposition
* Ton temps, c’est du logement.
* Du temps contre du logement.
* Tes compétences au service d'un territoire.
* Un village, un pro, un produit.

# Metaphor
La résidence d'artiste de l'artisan du logiciel

# Solution
1. Résidence d'entrepreneur du numérique
2. Échanges directs avec les utilisateurs
3. Lien fort avec un territoire

# Channels
- Cercle 1
- Cercle 2

# Cost-Structure
- Temps à passer à vendre et convaincre

# Revenue-Stream
- Bénévolat

# Key-Metrics
1. Satisfaction (NPS) des pros
2. Satisfaction (NPS) des villages
3. Nombre de jours d'hébergement

# Unfair-advantage
- Bénévolat
- Réputation liée à la modernisation de l'action publique
- Lien privilégié avec un premier village
- Expérience de la prestation d'indépendant dans le secteur public
