---
layout: post
title:  "Villages et développeurs"
---
L'enjeu majeur de notre temps est écologique. Les conditions d'habitabilité de cette planète seront dans un avenir proche de plus en plus difficiles. Nous sommes convaincus qu'à ce moment là, celles et ceux qui s'en sortiront sont :
- les personnes déjà inscrites dans des réseaux de solidarité
- les personnes qui sont à l'aise avec la frugalité

Où se trouvent aujourd'hui ces personnes : principalement dans les villages. Nous pouvons donc affirmer haut et fort que les villages sont l'avenir ! C'est donc dans ces territoires que nos efforts doivent être portés.

_Villages en commun_ a la même intention que la mission interministerielle [betagouv](https://beta.gouv.fr) : nous avons l'ambition de résoudre de véritables problèmes, avec une frugalité de moyens, en mode agile, dirigé par une métrique d'impact et de véritables utilisateurs.

La spécificité, c'est qu'à l'échelle d'une petite commune, le modèle économique doit être adapté. Nous ne trouverons pas 200k€ pour commencer à construire un service public numérique villageois. Même si nous arrivions à réunir et intéresser 100 villages à une même problématique, nous n'arriverions pas à les aligner pour une prise de risque financiere initiale.

Les talents du numérique coûtent trop cher. Alors, qu'ont les villages à offrir ? Si possible en s'adressant aux talents du numérique qui ont une appétence sincère à aider spécifiquement les petits territoires localisés. En fait, on se rapproche de la niche #DigitalNomad (ou expatrié d'une grande métropole en manque de verdure). L'on pourrait échanger des compétences et du temps de développement, d'expérience utilisateur contre de l'espace et du logement, sur le modèle de la résidence d'artiste mais avec un objectif de production de service public.

La visée de gouvernance serait une organisation en fédération des acteurs locaux plutôt qu'une centralisation dans un ministère par définition hors-sol. Cela exclut son financement du même coup.

Le troc plutôt que le paiement pourrait fonctionner pour la phase d'investigation et de construction, mais pour passer à l'échelle, de l'argent sera nécessaire. Peut-être qu'une structure comme l'Association des Maires Ruraux de France serait légitime à porter une demande de soutien à la Banque des Territoires ?

- Vous êtes un village et cette idée vous intéresse ? [Envoyez-nous un mail !](mailto:equipe@villages-en-commun.org)
- Vous travaillez dans le développement logiciel et cette idée vous intéresse ? [Envoyez-nous un mail !](mailto:equipe@villages-en-commun.org)

